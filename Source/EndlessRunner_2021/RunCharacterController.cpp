// Fill out your copyright notice in the Description page of Project Settings.


#include "RunCharacterController.h"
#include "RunCharacter.h"

ARunCharacterController::ARunCharacterController() {}

void ARunCharacterController::BeginPlay()
{
	Super::BeginPlay();

	
	runCharacter = Cast<ARunCharacter>(GetPawn());
}

void ARunCharacterController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//move forward while alive
	if (runCharacter->_isDead == false) MoveForward();

	
}

void ARunCharacterController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAxis("MoveRight", this, &ARunCharacterController::MoveRight);
}

void ARunCharacterController::MoveRight(float input)
{
	//GEngine->AddOnScreenDebugMessage(-1, 0, FColor::Red, FString::SanitizeFloat(input));
	runCharacter->AddMovementInput(FVector(0, input , 0), 1);
}

void ARunCharacterController::MoveForward()
{
	runCharacter->AddMovementInput(FVector(1, 0, 0), 1);
}


