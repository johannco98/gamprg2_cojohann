// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Tile.generated.h"
class UArrowComponent;
class UBoxComponent;
class ATileSpawner;
class AObstacle;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnExitDelegate, ATile*, Tile);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDestroy);

UCLASS()
class ENDLESSRUNNER_2021_API ATile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATile();

	UPROPERTY(BlueprintAssignable)
	FOnExitDelegate onExit;

	FTimerHandle handle1;
	ATileSpawner* spawnerHandler;
	FOnDestroy OnDestroy;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	//meta privateacess blueprint only
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	USceneComponent* sceneComponent;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	UArrowComponent* arrow;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	UBoxComponent* box;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	UBoxComponent* spawnArea;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<TSubclassOf<class AObstacle>> ObstacleToSpawn;


	UFUNCTION()
		void SpawnObstacle();

	UFUNCTION()
	void OnBoxOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, 
						UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, 
						const FHitResult& SweepResult);
	UFUNCTION()
	void DestroyBox();

	//getter in header, forces compiler to declare in header file
	FORCEINLINE UArrowComponent* getArrow ()
	{
		return arrow;
	}
};
