// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "RunCharacterController.generated.h"

class ARunCharacter;

UCLASS()
class ENDLESSRUNNER_2021_API ARunCharacterController : public APlayerController
{
	GENERATED_BODY()

public:
	ARunCharacterController();

	ARunCharacter* runCharacter;

protected:
	virtual void BeginPlay() override;

public: 
	virtual void Tick(float DeltaTime) override;
	virtual void SetupInputComponent() override;
	void MoveRight(float input);
	void MoveForward();
};
