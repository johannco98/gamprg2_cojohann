// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RunGameMode.generated.h"


class ATile;

UCLASS()
class ENDLESSRUNNER_2021_API ARunGameMode : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	ARunGameMode();

protected:
	virtual void BeginPlay() override;

	

public:
	virtual void Tick(float DeltaTime) override;
	UFUNCTION(BlueprintImplementableEvent)
	void RunCharacterDeath(ARunCharacter* RunCharacter);
	
	UFUNCTION()
	void OnRunCharacterDeath(class ARunCharacter* RunCharacter);
	
};
