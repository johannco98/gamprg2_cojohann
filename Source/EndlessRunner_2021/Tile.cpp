// Fill out your copyright notice in the Description page of Project Settings.


#include "Tile.h"
#include "Components/ArrowComponent.h"
#include "Components/BoxComponent.h"
#include "TimerManager.h"
#include "RunGameMode.h"
#include "TileSpawner.h"
#include "Kismet/KismetMathLibrary.h"
#include "Math/UnrealMathUtility.h"
#include "Obstacle.h"
#include "RunCharacter.h"

// Sets default values
ATile::ATile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	sceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene Component"));
	SetRootComponent(sceneComponent);
	
	arrow = CreateDefaultSubobject<UArrowComponent>(TEXT("Arrow Component"));
	arrow->SetupAttachment(sceneComponent);

	box = CreateDefaultSubobject<UBoxComponent>(TEXT("Box Component"));
	box->SetupAttachment(sceneComponent);

	spawnArea = CreateDefaultSubobject<UBoxComponent>(TEXT("Where to Spawn"));
	spawnArea->SetupAttachment(sceneComponent);
}


// Called when the game starts or when spawned
void ATile::BeginPlay()
{
	Super::BeginPlay();

	int32 randInt = FMath::FRandRange(1, 3);
	for (int i = 0; i <= randInt; i++)
	{
		SpawnObstacle();
	}
	box->OnComponentBeginOverlap.AddDynamic(this, &ATile::OnBoxOverlap);
}

// Called every frame
void ATile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATile::SpawnObstacle()
{
	const FVector SpawnOrigin = spawnArea->Bounds.Origin;
	const FVector SpawnExtent = spawnArea->Bounds.BoxExtent;

	int32 randInt = FMath::FRandRange(0, ObstacleToSpawn.Num()-1);

	AObstacle* SpawnObstacle = GetWorld()->SpawnActor<AObstacle>(ObstacleToSpawn[randInt], 
	UKismetMathLibrary::RandomPointInBoundingBox(SpawnOrigin, SpawnExtent), FRotator::ZeroRotator);
	OnDestroy.AddDynamic(SpawnObstacle, &AObstacle::DeleteOnExit);

}

void ATile::OnBoxOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (Cast<ARunCharacter>(OtherActor))
	{
		onExit.Broadcast(this);
		FTimerDelegate TimerDelegate;
		TimerDelegate.BindUFunction(this, FName("DestroyBox"));
		GetWorld()->GetTimerManager().SetTimer(handle1, TimerDelegate, 1.5f, false);
	}
}

void ATile::DestroyBox()
{
	OnDestroy.Broadcast();
	Destroy();
}

