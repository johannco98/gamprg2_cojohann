// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TileSpawner.generated.h"
class ATile;

UCLASS()
class ENDLESSRUNNER_2021_API ATileSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATileSpawner();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	//Instantiate
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<ATile> TileToSpawn;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<ATile*> AllTileSpawned;

	UFUNCTION()
		void SpawnTiles();

	UFUNCTION()
		void OnTileExit(ATile* tile);
};
