// Fill out your copyright notice in the Description page of Project Settings.


#include "TileSpawner.h"
#include "Tile.h"
#include "Components/ArrowComponent.h"
// Sets default values
ATileSpawner::ATileSpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATileSpawner::BeginPlay()
{
	Super::BeginPlay();
	for (int32 i = 0; i < 9; i++)
	{
		SpawnTiles();
	}
}

// Called every frame
void ATileSpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATileSpawner::SpawnTiles()
{
	if (AllTileSpawned.Num() == 0)
	{
		ATile* SpawnTile = GetWorld()->SpawnActor<ATile>(TileToSpawn);
		SpawnTile->onExit.AddDynamic(this, &ATileSpawner::OnTileExit);
		AllTileSpawned.Add(SpawnTile);
	}

	else
	{
		ATile* LastSpawned = AllTileSpawned[AllTileSpawned.Num() - 1];
		LastSpawned->getArrow();
		FVector location = LastSpawned->getArrow()->GetComponentLocation();

		ATile* SpawnTile = GetWorld()->SpawnActor<ATile>(TileToSpawn, location, FRotator::ZeroRotator);
		SpawnTile->onExit.AddDynamic(this, &ATileSpawner::OnTileExit);
		AllTileSpawned.Add(SpawnTile);
	}
}

void ATileSpawner::OnTileExit(ATile* tile)
{
	AllTileSpawned.Remove(tile);
	SpawnTiles();
}

