// Fill out your copyright notice in the Description page of Project Settings.


#include "RunGameMode.h"
#include "RunCharacter.h"

ARunGameMode::ARunGameMode()
{
}

void ARunGameMode::BeginPlay()
{
	Super::BeginPlay();

	if (ARunCharacter* runCharacter = Cast<ARunCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn()))
	{
		runCharacter->OnDeath.AddDynamic(this, &ARunGameMode::OnRunCharacterDeath);
	};
}

// Called every frame
void ARunGameMode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ARunGameMode::OnRunCharacterDeath(ARunCharacter* RunCharacter)
{
	RunCharacterDeath(RunCharacter);
}
