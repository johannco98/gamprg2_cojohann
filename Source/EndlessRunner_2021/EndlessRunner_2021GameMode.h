// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "EndlessRunner_2021GameMode.generated.h"

UCLASS(minimalapi)
class AEndlessRunner_2021GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AEndlessRunner_2021GameMode();
};



