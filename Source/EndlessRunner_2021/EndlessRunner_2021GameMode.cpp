// Copyright Epic Games, Inc. All Rights Reserved.

#include "EndlessRunner_2021GameMode.h"
#include "EndlessRunner_2021Character.h"
#include "UObject/ConstructorHelpers.h"

AEndlessRunner_2021GameMode::AEndlessRunner_2021GameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
